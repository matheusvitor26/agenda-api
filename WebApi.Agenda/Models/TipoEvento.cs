﻿using System;
using System.Collections.Generic;

namespace WebApi.Agenda
{
    public partial class TipoEvento
    {
        public TipoEvento()
        {
            Eventos = new HashSet<Eventos>();
        }

        public int TipoEventoId { get; set; }
        public string Descricao { get; set; }

        public virtual ICollection<Eventos> Eventos { get; set; }
    }
}
