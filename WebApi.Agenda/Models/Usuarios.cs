﻿using System;
using System.Collections.Generic;

namespace WebApi.Agenda
{
    public partial class Usuarios
    {
        public Usuarios()
        {
            Participantes = new HashSet<Participantes>();
        }

        public int UsuarioId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public byte[] Senha { get; set; }

        public virtual ICollection<Participantes> Participantes { get; set; }
    }
}
