﻿using System;
using System.Collections.Generic;

namespace WebApi.Agenda
{
    public partial class Eventos
    {
        public Eventos()
        {
            Participantes = new HashSet<Participantes>();
        }

        public int EventoId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Local { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int TipoEventoId { get; set; }

        public virtual TipoEvento TipoEvento { get; set; }
        public virtual ICollection<Participantes> Participantes { get; set; }
    }
}
