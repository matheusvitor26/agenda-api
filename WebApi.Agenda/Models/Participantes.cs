﻿using System;
using System.Collections.Generic;

namespace WebApi.Agenda
{
    public partial class Participantes
    {
        public int Id { get; set; }
        public int EventoId { get; set; }
        public int UsuarioId { get; set; }
        public bool FlgOrganizador { get; set; }

        public virtual Eventos Evento { get; set; }
        public virtual Usuarios Usuario { get; set; }
    }
}
