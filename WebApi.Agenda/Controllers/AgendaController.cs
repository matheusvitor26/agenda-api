﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Agenda.DTO;

namespace WebApi.Agenda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AgendaController : Controller //ControllerBase
    {
        private readonly AgendaContext _context = new AgendaContext();

        /// <summary>
        /// Exibir os eventos do usuário
        /// </summary>
        /// <param name="start">Data inicial</param>
        /// <param name="end">Data final</param>
        /// <param name="user">Usuário da agenda</param>
        /// <returns>Lista de eventos do usuário</returns>
        [HttpGet("GetEventos")]
        public JsonResult GetEventos(DateTime start, DateTime end, int user)
        {
            try
            {
                return Json((from e in _context.Eventos
                             join p in _context.Participantes on e.EventoId equals p.EventoId
                             join t in _context.TipoEvento on e.TipoEventoId equals t.TipoEventoId
                             where e.DataInicio.Date >= start.Date && e.DataFim.Date <= end.Date && p.UsuarioId == user
                             select new EventosUsuarioDTO
                             {
                                 Id = e.EventoId,
                                 Title = e.Nome,
                                 Start = e.DataInicio,
                                 End = e.DataFim,
                                 Descricao = e.Descricao,
                                 Local = e.Local,
                                 TipoEvento = t.Descricao
                             }).ToList());
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { mensagem = ex.Message });
            }
        }

        /// <summary>
        /// Exibir todos os participantes ou somente de um evento
        /// </summary>
        /// <returns>Lista de participantes do evento</returns>
        [HttpGet("GetParticipantes")]
        public JsonResult GetParticipantes(int idEvento)
        {
            try
            {
                if (idEvento.Equals(0))
                {
                    return Json((from e in _context.Eventos
                                 join p in _context.Participantes on e.EventoId equals p.EventoId
                                 join u in _context.Usuarios on p.UsuarioId equals u.UsuarioId
                                 select new ParticipantesDTO
                                 {
                                     Id = u.UsuarioId,
                                     Nome = u.Nome
                                 }).Distinct().ToList());
                }
                else
                {
                    return Json((from e in _context.Eventos
                                 join p in _context.Participantes on e.EventoId equals p.EventoId
                                 join u in _context.Usuarios on p.UsuarioId equals u.UsuarioId
                                 where e.EventoId == idEvento //&& p.FlgOrganizador == false
                                 select new ParticipantesDTO
                                 {
                                     Id = u.UsuarioId,
                                     Nome = u.Nome
                                 }).Distinct().ToList());
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { mensagem = ex.Message });
            }
        }

        /// <summary>
        /// Exibir os tipos de evento
        /// </summary>
        /// <returns>Lista de tipos de evento</returns>
        [HttpGet("GetTipoEvento")]
        public JsonResult GetTipoEvento()
        {
            try
            {
                return Json((from t in _context.TipoEvento
                             select new
                             {
                                 t.TipoEventoId,
                                 t.Descricao
                             }).Distinct().ToList());
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { mensagem = ex.Message });
            }
        }


        [HttpGet("SalvarEvento")]
        public JsonResult SalvarEvento([FromBody] EventosUsuarioDTO evento)
        {
            try
            {


                return Json(new { message = "" });
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { mensagem = ex.Message });
            }
        }
    }
}
