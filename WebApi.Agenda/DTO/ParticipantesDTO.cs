﻿namespace WebApi.Agenda.DTO
{
    public class ParticipantesDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
