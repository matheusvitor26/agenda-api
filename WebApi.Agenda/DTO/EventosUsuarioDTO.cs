﻿using System;

namespace WebApi.Agenda.DTO
{
    public class EventosUsuarioDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Descricao { get; set; }
        public string Local { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string TipoEvento { get; set; }
    }
}
